<?php
namespace Com\Daw2\Helpers;
/* 
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

class UsuarioSistema{
    private $idUsuario;
    private $rol;
    private $email;
    private $nombre;
    private $idioma;
    private $baja;
    
    //private function __construct(){}
    
    public function __construct(?int $idUsuario, Rol $rol, string $email, string $nombre, string $idioma, string $baja){
        $this->idUsuario = $idUsuario;
        $this->rol = $rol;
        $this->email = $email;
        $this->nombre = $nombre;
        $this->idioma = $idioma;
        $this->baja = $baja;
    }        
    
    public function getIdUsuario() : int{
        return $this->idUsuario;
    }

    public function getRol() : Rol {
        return $this->rol;
    }

    public function getEmail() : string{
        return $this->email;
    }

    public function getNombre() : string {
        return $this->nombre;
    }

    public function getIdioma() : string{
        return $this->idioma;
    }

    public function getBaja() : bool{
        return $this->baja;
    }

    public function setRol(Rol $rol): void {
        $this->rol = $rol;
    }

    public function setEmail(string $email): void {
        $this->email = $email;
    }

    public function setNombre(string $nombre): void {
        $this->nombre = $nombre;
    }

    public function setIdioma(string $idioma): void {
        $this->idioma = $idioma;
    }

    public function setBaja(bool $baja): void {
        $this->baja = $baja;
    }

}

