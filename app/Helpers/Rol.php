<?php
namespace Com\Daw2\Helpers;
/* 
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

class Rol{
    private $idRol;
    private $rol;
    private $descripcionEs;
    private $descripcionEn;
    private $_permisos;
    
    private static $_PERMISOS_ADMIN = array(
            'UsuariosSistema' => ['r', 'w', 'x', 'd']
    );    
    
    public function __construct(int $idRol, string $rol, string $descripcionEs, string $descripcionEn) {
        $this->descripcionEn = $descripcionEn;
        $this->descripcionEs = $descripcionEs;
        $this->idRol = $idRol;
        $this->$rol = $rol;
        if($idRol == 1){
            $this->_permisos = self::$_PERMISOS_ADMIN;
        }
    }
    
    public function getIdRol() : int{
        return $this->idRol;
    }

    public function getRol() : string{
        return $this->rol;
    }

    public function getDescripcionEs() : string {
        return $this->descripcionEs;
    }

    public function getDescripcionEn() : string{
        return $this->descripcionEn;
    }
    
    public function checkPermiso(string $controller, string $operacion) : bool{
        if(isset($this->_permisos[$controller])){
            return in_array($operacion, $this->_permisos[$operacion]);
        }
        else{
            return false;
        }
    }

}