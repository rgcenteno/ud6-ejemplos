-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Xerado en: 21 de Nov de 2022 ás 14:23
-- Versión do servidor: 10.3.34-MariaDB-0ubuntu0.20.04.1
-- Versión do PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `CS_06_autenticacion`
--

-- --------------------------------------------------------

--
-- Estrutura da táboa `rol`
--

CREATE TABLE `rol` (
  `id_rol` int(11) NOT NULL,
  `rol` varchar(255) DEFAULT NULL,
  `descripcion_en` text DEFAULT NULL,
  `descripcion_es` text DEFAULT NULL,
  `orden` int(255) DEFAULT NULL,
  `seccion_ini` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- A extraer os datos da táboa `rol`
--

INSERT INTO `rol` (`id_rol`, `rol`, `descripcion_en`, `descripcion_es`, `orden`, `seccion_ini`) VALUES
(1, 'Administrator', 'Can access without restrictions. Can manage all the data of the application including master data tables.', 'Acceso a toda la aplicación.\r\nLa única  restricción es que no pueden modificar el usuario  superadmin.', 3, 'users'),
(2, 'Organisation coordinator', 'Can access and edit all the data of the his organisation users. Can view and edit all the financial data of the organisation. ', 'Pueden crear  personas/expedientes y gestionar los datos de las mismas.', 1, 'users'),
(3, 'Staff', 'Only can edit his own data and his input hours.', 'Sólo puede trabajar sobre sus operaciones.', NULL, 'jobs');

-- --------------------------------------------------------

--
-- Estrutura da táboa `usuario_sistema`
--

CREATE TABLE `usuario_sistema` (
  `id_usuario` int(11) NOT NULL,
  `id_rol` int(11) DEFAULT 1,
  `email` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `last_date` datetime DEFAULT NULL,
  `idioma` char(2) DEFAULT 'es',
  `baja` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- A extraer os datos da táboa `usuario_sistema`
--

INSERT INTO `usuario_sistema` (`id_usuario`, `id_rol`, `email`, `pass`, `nombre`, `last_date`, `idioma`, `baja`) VALUES
(1, 1, 'admin@test.org', '$2y$10$C99Nf4Ph.iOIsCQTHUmSbefDUsskOBkx9ciM0.LzlBGAdAmQFQVCC', 'Administrador', '2021-09-11 18:36:42', 'es', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indexes for table `usuario_sistema`
--
ALTER TABLE `usuario_sistema`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `FK_usuario_rol` (`id_rol`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rol`
--
ALTER TABLE `rol`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `usuario_sistema`
--
ALTER TABLE `usuario_sistema`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- Restricións para os envorcados das táboas
--

--
-- Restricións para a táboa `usuario_sistema`
--
ALTER TABLE `usuario_sistema`
  ADD CONSTRAINT `FK_usuario_rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id_rol`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
